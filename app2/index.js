require('dotenv').config()
const express = require('express')
const app = express()
const port = parseInt(process.env.APP2_PORT)

app.get('/health-check', (req, res) => {
  res.json({
      alive: 'ALIVE',
      online: true,
      timeISO: (new Date()).toISOString(),
      time: Date.now(),
  })
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})
