const axios = require('axios');

module.exports = {
    healthCheck
}

async function healthCheck() {
        
    const status = await axios.get(`${process.env.ADRESA_APP2}/health-check`);
    console.log(status.data)
}