require('dotenv').config()
const express = require('express')
const app = express()
const port = parseInt(process.env.APP1_PORT)
//const app2Client = require('./app2-client/index.js')
const app2Client = require('./app2-client')

// DA LI JE APP2 ziva 
// 
// APP2 klijent
// IMPLEMENTACIJA kada smo MI KLIJENT KOJI GADJA APP2
// nas klijent za gadjanje app2
try {
    app2Client.healthCheck();
} catch(e) {
    console.error(`App2 nije dostpupna, ne moze da radimo bez app2`)
    process.exit(1);
}


app.get('/', (req, res) => {
  res.json({
      id: 1,
      user: {
          name: 'John',
          lastname: 'Doe'
      }
  });
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})