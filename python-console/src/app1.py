import os
import servis_sabiranje
import provera_config
import servis_sabiranje_tri_broja

def main():
    provera_config.proveri()
    print('Program za sabiranje brojeva')
    prvi_broj = int(os.environ['PRVI_BROJ'])
    drugi_broj = int(os.environ['DRUGI_BROJ'])

    rezultat = servis_sabiranje.saberi(prvi_broj, drugi_broj)
    print(f'Rezul;tat je {rezultat}')


    #napraviti svoje env variajble
    # prefiks je NAZIV_SERVISA_nesto
    # nAZXIV_SERVISA_PRVI
    # ZA WINDOWS JE SET SERVIS_ZA_TRI_DRUGI_BROJ=1
    # za linux/  export SERVIS_ZA_TRI_DRUGI_BROJ=2
    
    prvi_broj_servis3 = int(os.environ['SERVIS_ZA_TRI_PRVI_BROJ'])
    drugi_broj_servis3 = int(os.environ['SERVIS_ZA_TRI_DRUGI_BROJ'])
    treci_broj_servis3 = int(os.environ['SERVIS_ZA_TRI_TRECI_BROJ'])
    rezultat_servis3 = servis_sabiranje_tri_broja.saberi_tri(prvi_broj_servis3, drugi_broj_servis3, treci_broj_servis3)
    print(f'Rezultat3 je {rezultat_servis3}')

main()

# TEST promena app1 23123
