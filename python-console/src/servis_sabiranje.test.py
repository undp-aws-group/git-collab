import servis_sabiranje

def main():
    ocekivano = 2
    dobijeno = servis_sabiranje.saberi(1, 1)
    if ocekivano == dobijeno:
        print(f'PASS Rezultat je: {dobijeno}')
    else:
        print(f'FAIL Rezulat je {dobijeno}, ocekivano {ocekivano}')

main()