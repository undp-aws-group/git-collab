import os
import sys

spisak_varijabli = [
    'PRVI_BROJ', 
    'DRUGI_BROJ',
    'SERVIS_ZA_TRI_PRVI_BROJ',
    'SERVIS_ZA_TRI_DRUGI_BROJ',
    'SERVIS_ZA_TRI_TRECI_BROJ'
]

def proveri():
    spisak_fali = []
    for naziv_varijable in spisak_varijabli:
        if os.environ.get(naziv_varijable) is None:
           spisak_fali.append(f'Fali varijable {naziv_varijable}')

    if len(spisak_fali) == 0:
        return
    
    for fali in spisak_fali:
        print(fali)
    
    sys.exit(1)

