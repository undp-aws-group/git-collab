import servis_sabiranje_tri_broja

def main():
    ocekivano = 3
    dobijeno = servis_sabiranje_tri_broja.saberi_tri(1, 1, 1)
    if ocekivano == dobijeno:
        print(f'PASS Rezultat je: {dobijeno}')
    else:
        print(f'FAIL Rezulat je {dobijeno}, ocekivano {ocekivano}')

main()